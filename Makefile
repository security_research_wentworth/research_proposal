SHELL := /bin/bash
build_dir="_latex_compile"

filename=main
# files must be in regex format #
files=".*\(sh\|ex\|png\|prob\|tex\|scripts\|c\)"

pdf: build_dir
	cd ${build_dir}; if [[ -f first_build.wow ]] ; then pdflatex -shell-escape ${filename}; rm first_build.wow; fi
	cd ${build_dir}; pdflatex -shell-escape ${filename}
	cp ${build_dir}/${filename}.pdf .

build_dir:
	if [[ ! -d ${build_dir} ]] ; then mkdir ${build_dir}; touch ${build_dir}/first_build.wow; fi
	find . -not -iwholename "*${build_dir}*" -not -iwholename "*.git*" -type f -regex ${files} -print0 | \
		xargs -0 cp -a -t ${build_dir}

read:
	evince ${filename}.pdf &>/dev/null &

clean:
	rm -f ${filename}.{pdf,dvi,log,aux,out,toc,snm,nav}
	rm -rf ${build_dir}


